import * as utils from 'test-utils';
import util from 'util';

const params = { password: 'sdf234fdsf32cdsc' };

const runMePleaseMe = util.promisify(utils.runMePlease);

try {
    const result = await runMePleaseMe(params);
    console.log(result);
}catch (error){
    console.log(error);
}